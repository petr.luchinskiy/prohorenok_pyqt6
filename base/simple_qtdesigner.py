from PyQt6 import QtWidgets, uic
import sys
from qt_material import apply_stylesheet

app = QtWidgets.QApplication(sys.argv)
window = uic.loadUi("simple window.ui")
apply_stylesheet(app, theme='dark_yellow.xml')
window.show()
sys.exit(app.exec())