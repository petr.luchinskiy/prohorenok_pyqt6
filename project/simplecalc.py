import sys
from PyQt6.QtWidgets import QApplication, QWidget, QLineEdit, QPushButton, QHBoxLayout, QVBoxLayout, QTextEdit
from PyQt6 import QtWidgets, QtCore, uic
from qt_material import apply_stylesheet


class MyWindow(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.input1 = QLineEdit(self)
        self.input2 = QLineEdit(self)
        self.output = QTextEdit(self)
        button = QPushButton('Сгенерировать', self)
        button.clicked.connect(self.generateResult)
        hbox1 = QHBoxLayout()
        hbox1.addWidget(self.input1)
        hbox1.addWidget(self.input2)

        hbox2 = QHBoxLayout()
        hbox2.addWidget(button)

        vbox = QVBoxLayout()
        vbox.addLayout(hbox1)
        vbox.addLayout(hbox2)
        vbox.addWidget(self.output)

        self.setLayout(vbox)

        self.setWindowTitle('Пример генерации результата')

        self.show()

    def generateResult(self):
        value1 = int(self.input1.text())
        value2 = int(self.input2.text())

        result = value1 * value2  # здесь можно провести необходимые вычисления

        self.output.setText(str(result))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    window = MyWindow()
    window.setWindowTitle('OOP syle create window')
    apply_stylesheet(app, theme='dark_purple.xml')
    sys.exit(app.exec())
