import sys

import pyodbc
import pypyodbc
from PyQt6.QtWidgets import QApplication, QWidget, QTableWidget, QTableWidgetItem, QVBoxLayout, QHBoxLayout, \
    QPushButton, QDialog, QMessageBox
from PyQt6.uic.properties import QtWidgets
from qt_material import apply_stylesheet

from shredder_template import Ui_MainWindow

mySQLServer = "laptem"
myDataBase = "Потерянные"

class MyWindow(QWidget):
    def __init__(self):
        super().__init__()

        # создаем экземпляр формы
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.select_for_sud.clicked.connect(self.loadTable_for_sud)
        self.ui.select_sud.clicked.connect(self.loadTable_sud)
        self.ui.del_for_sud.clicked.connect(self.deleteRow_for_sud)
        self.ui.del_sud.clicked.connect(self.deleteRow_sud)
        self.ui.table_sud.setColumnCount(39)
        self.ui.table_for_sud.setColumnCount(18)

        self.ui.table_sud.setHorizontalHeaderLabels(['ДоговорИд', 'АбонентИд', 'ФИО','ДолгС', 'ДатаДолга', 'СуммаДолга','СуммаПоСуду', 'Пеня', 'НомерРеестра',
                                                         'ГодРеестра', 'НомерСудПриказ', 'ДатаСудПриказ','СостояниеСуда', 'ДатаОплаты', 'УточнениеСудИд','Госпошлина', 'КомСбор', 'УслАдвоката','ПогашениеУслАдв', 'ДатаОплатыУслАдв', 'ЗаказПисьма',
                                                         'ОплатаЗП', 'ДатаОплатыЗП', 'ПогашениеГП','ДатаСнятияГП','СостояниеПени', 'ДатаОплатыПени', 'МестоРаботы','Примечание', 'СудИсполнитель', 'ДатаИзменения',
                                                         'СотрудникИд', 'Номер', 'ИНН','Исполнение', 'ФИОСудИсп', 'ИспДействия','ЧастОплата', 'ДатаОтпрИсп'])
        self.ui.table_for_sud.setHorizontalHeaderLabels(
            ['ДоговорИд', 'АбонентИд', 'ФИО', 'ДолгС', 'ДатаДолга', 'СуммаДолга', 'Пеня', 'СостояниеИд', 'Госпошлина',
             'КомСбор', 'УслАдвоката', 'ЗаказПисьма', 'МестоРаботы', 'Примечание', 'ДатаИзменения',
             'СотрудникИд', 'Номер', 'ИНН'])
        self.show()

    def loadTable_for_sud(self):
        self.ui.table_for_sud.clear()
        self.ui.table_for_sud.setHorizontalHeaderLabels(
            ['ДоговорИд', 'АбонентИд', 'ФИО', 'ДолгС', 'ДатаДолга', 'СуммаДолга', 'Пеня', 'СостояниеИд', 'Госпошлина',
             'КомСбор', 'УслАдвоката', 'ЗаказПисьма', 'МестоРаботы', 'Примечание', 'ДатаИзменения',
             'СотрудникИд', 'Номер', 'ИНН'])
        if self.ui.dogovorid_for_sud.text() == '':
            dogovorid = 0
        else:
            dogovorid = int(self.ui.dogovorid_for_sud.text())

        connection = pypyodbc.connect('Driver={SQL Server};'
                                      'Server=' + '192.168.154.250' + ';'
                                    'Database=' + myDataBase + ';')

        cursor = connection.cursor()

        # connection = pyodbc.connect('Driver={ODBC Driver 17 for SQL Server};'
        #                             'Server=192.168.154.250;'
        #                             'Database=Потерянные;'
        #                             'UID=nidxegg;'
        #                             'PWD=486255;')

        cursor = connection.cursor()
        cursor.execute(f"SELECT [ДоговорИд],[АбонентИд],[ФИО],[ДолгС],[ДатаДолга],[СуммаДолга],[Пеня]"
                       f",[СостояниеИд],[Госпошлина],[КомСбор],[УслАдвоката],[ЗаказПисьма],[МестоРаботы],[Примечание]"
                       f",[ДатаИзменения],[СотрудникИд],[Номер],[ИНН]  FROM [Потерянные].[dbo].[таб_ДляСуда]"
                       f"WHERE ДоговорИд = {dogovorid}")

        row_count = 0

        for row in cursor.fetchall():
            self.ui.table_for_sud.insertRow(row_count)
            for column in range(len(row)):
                self.ui.table_for_sud.setItem(row_count, column, QTableWidgetItem(str(row[column])))
            row_count += 1

        self.ui.table_for_sud.resizeColumnsToContents()
        connection.close()

    def loadTable_sud(self):
        self.ui.table_sud.clear()
        self.ui.table_sud.setHorizontalHeaderLabels(
            ['ДоговорИд', 'АбонентИд', 'ФИО', 'ДолгС', 'ДатаДолга', 'СуммаДолга', 'СуммаПоСуду', 'Пеня', 'НомерРеестра',
             'ГодРеестра', 'НомерСудПриказ', 'ДатаСудПриказ', 'СостояниеСуда', 'ДатаОплаты', 'УточнениеСудИд',
             'Госпошлина', 'КомСбор', 'УслАдвоката', 'ПогашениеУслАдв', 'ДатаОплатыУслАдв', 'ЗаказПисьма',
             'ОплатаЗП', 'ДатаОплатыЗП', 'ПогашениеГП', 'ДатаСнятияГП', 'СостояниеПени', 'ДатаОплатыПени',
             'МестоРаботы', 'Примечание', 'СудИсполнитель', 'ДатаИзменения',
             'СотрудникИд', 'Номер', 'ИНН', 'Исполнение', 'ФИОСудИсп', 'ИспДействия', 'ЧастОплата', 'ДатаОтпрИсп'])

        if self.ui.dogovorid_sud.text() == '':
            dogovorid = 0
        else:
            dogovorid = int(self.ui.dogovorid_sud.text())

        connection = pypyodbc.connect('Driver={SQL Server};'
                                      'Server=' + '192.168.154.250' + ';'
                                    'Database=' + myDataBase + ';')

        cursor = connection.cursor()
        cursor.execute(f"SELECT  [ДоговорИд] ,[АбонентИд],[ФИО],[ДолгС],[ДатаДолга],[СуммаДолга],[СуммаПоСуду] ,[Пеня] ,[НомерРеестра],"
                       f"[ГодРеестра]     ,[НомерСудПриказ],[ДатаСудПриказ],[СостояниеСуда],[ДатаОплаты],[УточнениеСудИд] ,[Госпошлина],[КомСбор],"
                       f"[УслАдвоката],[ПогашениеУслАдв],[ДатаОплатыУслАдв],[ЗаказПисьма],[ОплатаЗП],[ДатаОплатыЗП],[ПогашениеГП],[ДатаСнятияГП]"
                       f",[СостояниеПени],[ДатаОплатыПени],[МестоРаботы],[Примечание],[СудИсполнитель],[ДатаИзменения],[СотрудникИд],[Номер],[ИНН]"
                       f",[Исполнение],[ФИОСудИсп],[ИспДействия],[ЧастОплата],[ДатаОтпрИсп] FROM [Потерянные].[dbo].[таб_Суды]"
                       f"WHERE ДоговорИд = {dogovorid}")

        row_count = 0

        for row in cursor.fetchall():
            self.ui.table_sud.insertRow(row_count)
            for column in range(len(row)):
                self.ui.table_sud.setItem(row_count, column, QTableWidgetItem(str(row[column])))
            row_count += 1

        self.ui.table_sud.resizeColumnsToContents()
        connection.close()

    def deleteRow_for_sud(self):
        selected = self.ui.table_for_sud.selectedIndexes()
        if selected:

            row = selected[0].row()
            reply = QMessageBox()

            row_values = [self.ui.table_for_sud.item(row, col).text() for col in range(self.ui.table_for_sud.columnCount())]
            reply.setText(f"Снести ДолгС {row_values[3]} c суммой {row_values[5]}?   ")
            reply.setStandardButtons(QMessageBox.StandardButton.Yes |
                                     QMessageBox.StandardButton.No)
            x = reply.exec()

            if x == QMessageBox.StandardButton.Yes:
                row = selected[0].row()
                selected_items = self.ui.table_for_sud.selectedItems()
                if selected_items:
                    connection = pypyodbc.connect('Driver={SQL Server};'
                                                  'Server=' + '192.168.154.250' + ';'
                                                                                  'Database=' + myDataBase + ';')

                    cursor = connection.cursor()
                    cursor.execute("DELETE FROM таб_ДляСуда WHERE ДоговорИд=? and СуммаДолга =? ",
                                   (int(row_values[0]), row_values[5]))
                    cursor.commit()
                    connection.close()
                    self.ui.table_for_sud.removeRow(row)


    def deleteRow_sud(self):
        selected = self.ui.table_sud.selectedIndexes()
        if selected:
            row = selected[0].row()
            reply = QMessageBox()

            row_values = [self.ui.table_sud.item(row, col).text() for col in range(self.ui.table_sud.columnCount())]
            reply.setText(f"Снести ДолгС {row_values[3]} c суммой {row_values[5]}?   ")
            reply.setStandardButtons(QMessageBox.StandardButton.Yes |
                                     QMessageBox.StandardButton.No)

            x = reply.exec()

            if x == QMessageBox.StandardButton.Yes:
                selected_items = self.ui.table_sud.selectedItems()
                if selected_items:
                    #row_values = [self.ui.table_sud.item(row, col).text() for col in range(self.ui.table_sud.columnCount())]
                    connection = pypyodbc.connect('Driver={SQL Server};'
                                                  'Server=' + '192.168.154.250' + ';'
                                                                                  'Database=' + myDataBase + ';')

                    cursor = connection.cursor()
                    cursor.execute("DELETE FROM таб_Суды WHERE ДоговорИд=? and СуммаДолга =? ", (int(row_values[0]), row_values[5]))
                    cursor.commit()
                    connection.close()
                    self.ui.table_sud.removeRow(row)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MyWindow()
    apply_stylesheet(app, theme='dark_lightgreen.xml')
    sys.exit(app.exec())




