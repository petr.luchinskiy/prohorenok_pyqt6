from PyQt6.QtWidgets import *
from PyQt6.QtCore import *


class MyWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.table = QTableWidget()
        self.table.setColumnCount(3)
        self.table.setRowCount(5)
        self.table.setHorizontalHeaderLabels(["Name", "Age", "City"])

        for i in range(5):
            self.table.setItem(i, 0, QTableWidgetItem("John"))
            self.table.setItem(i, 1, QTableWidgetItem("25"))
            self.table.setItem(i, 2, QTableWidgetItem("New York"))

        layout = QVBoxLayout()
        layout.addWidget(self.table)
        self.setLayout(layout)

        self.table.cellDoubleClicked.connect(self.delete_row)

    def delete_row(self, row, column):
        item = self.table.item(row, 0)
        text = item.text()

        reply = QMessageBox.question(self, 'Подтверждение', f'Вы действительно хотите удалить запись "{text}"?',
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            self.table.removeRow(row)


app = QApplication([])
win = MyWindow()
win.show()
app.exec()