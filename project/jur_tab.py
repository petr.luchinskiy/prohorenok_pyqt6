import sys
import pyodbc
from PyQt6.QtWidgets import QApplication, QWidget, QTableWidget, QTableWidgetItem, QVBoxLayout, QHBoxLayout, QPushButton


class MyWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.table = QTableWidget(self)
        self.table.setColumnCount(3)
        self.table.setHorizontalHeaderLabels(['id', 'Название', 'Город'])

        self.loadTable()

        self.deleteButton = QPushButton('Удалить', self)
        self.deleteButton.clicked.connect(self.deleteRow)

        hbox = QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(self.deleteButton)

        vbox = QVBoxLayout(self)
        vbox.addWidget(self.table)
        vbox.addLayout(hbox)

        self.setWindowTitle('Удаление выделенной строки в QTableWidget')
        self.show()

        vbox = QVBoxLayout()
        vbox.addWidget(self.table)

        self.setLayout(vbox)
        self.setWindowTitle('Выборка данных из MS SQL Server')

        self.show()

    def loadTable(self):
        connection = pyodbc.connect('Driver={ODBC Driver 17 for SQL Server};'
                                    'Server=laptem;'
                                    'Database=Потерянные;'
                                    'UID=nidxegg;'
                                    'PWD=486255;')

        cursor = connection.cursor()
        cursor.execute('SELECT TOP (1000) [ПКСКИд]      ,[НазваниеПКСК]  FROM [Потерянные].[dbo].[таб_ПКСК]')

        row_count = 0

        for row in cursor.fetchall():
            self.table.insertRow(row_count)
            for column in range(len(row)):
                self.table.setItem(row_count, column, QTableWidgetItem(str(row[column])))
            row_count += 1

        self.table.resizeColumnsToContents()

    def deleteRow(self):
        selected = self.table.selectedIndexes()
        if selected:
            row = selected[0].row()
            self.table.removeRow(row)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyWindow()
    sys.exit(app.exec())
